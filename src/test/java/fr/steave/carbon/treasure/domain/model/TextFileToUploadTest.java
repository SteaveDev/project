package fr.steave.carbon.treasure.domain.model;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TextFileToUploadTest {
    private static final String VALID_FILE_CONTENT_TYPE = "text/plain";
    private static final String INVALID_FILE_CONTENT_TYPE = "application/octet-stream";
    private static final byte[] BYTES = "Hello World !".getBytes();

    @Test
    void givenInvalidContentType_creatingFileToUpload_shouldGenerateAnErrorMessage() {
        // WHEN THEN
        final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> new TextFileToUpload(INVALID_FILE_CONTENT_TYPE, BYTES));

        assertThat(exception).hasMessage("Le fichier n'est pas un fichier texte.");
    }

    @Test
    void givenValidContentType_creatingFileToUpload_shouldCreateFile() {
        // WHEN
        final TextFileToUpload textFileToUpload = new TextFileToUpload(VALID_FILE_CONTENT_TYPE, BYTES);

        // THEN
        assertThat(textFileToUpload).isNotNull();
    }

}
