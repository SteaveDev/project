package fr.steave.carbon.treasure.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class MountainTest {

    @Test
    void givenValidInformation_whenCreateMountain_thenMountainIsCreated() {
        // GIVEN
        final String[] info = {"M", "3", "4"};

        // WHEN
        final Mountain mountain = Mountain.createMountain(info);

        // THEN
        assertEquals(3, mountain.getHorizontal());
        assertEquals(4, mountain.getVertical());
    }

    @Test
    void givenMountain_whenToString_thenCorrectStringIsReturned() {
        // GIVEN
        final Mountain mountain = new Mountain(3, 4);

        // WHEN THEN
        assertEquals("M", mountain.toString());
    }

    @Test
    void givenTwoMountains_whenEqualsAndHashCode_thenCorrectComparison() {
        // GIVEN
        final Mountain mountain1 = new Mountain(3, 4);
        final Mountain mountain2 = new Mountain(3, 4);
        final Mountain mountain3 = new Mountain(5, 6);

        // WHEN THEN
        assertEquals(mountain1, mountain2);
        assertNotEquals(mountain1, mountain3);
        assertEquals(mountain1.hashCode(), mountain2.hashCode());
        assertNotEquals(mountain1.hashCode(), mountain3.hashCode());
    }
}
