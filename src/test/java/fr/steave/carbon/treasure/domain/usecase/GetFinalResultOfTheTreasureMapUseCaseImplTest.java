package fr.steave.carbon.treasure.domain.usecase;

import fr.steave.carbon.treasure.domain.api.MapGameFileService;
import fr.steave.carbon.treasure.domain.model.Coordinate;
import fr.steave.carbon.treasure.domain.model.MyMap;
import fr.steave.carbon.treasure.domain.model.TextFileToUpload;
import fr.steave.carbon.treasure.domain.spi.DocumentStorageSpi;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Collections;
import java.util.List;
import java.util.Map;

import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.A;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.C;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.M;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.T;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GetFinalResultOfTheTreasureMapUseCaseImplTest {

    @Mock
    private MapGameFileService mapGameFileService;

    @Mock
    private DocumentStorageSpi documentStorageSpi;

    @InjectMocks
    private GetFinalResultOfTheTreasureMapUseCaseImpl getFinalResultOfTheTreasureMapUseCaseImpl;


    @Test
    void givenTextFileToUpload_execute_shouldProcessAndUploadFinalResult() {
        // GIVEN
        final TextFileToUpload textFileToUpload = new TextFileToUpload("text/plain", "C - 3 - 4".getBytes());
        final Map<String, List<Coordinate>> coordinatesMap = Map.of(
                C, List.of(new MyMap(3, 4)),
                M, Collections.emptyList(),
                T, Collections.emptyList(),
                A, Collections.emptyList()
        );

        when(mapGameFileService.filterAndParseCoordinates(any(TextFileToUpload.class))).thenReturn(coordinatesMap);

        // WHEN
        getFinalResultOfTheTreasureMapUseCaseImpl.execute(textFileToUpload);

        // THEN
        verify(mapGameFileService).filterAndParseCoordinates(textFileToUpload);
        verify(documentStorageSpi).uploadTextFile(eq("result_file"), anyString());
    }
}
