package fr.steave.carbon.treasure.domain.model;

import fr.steave.carbon.treasure.domain.exception.MapGameClassException;
import fr.steave.carbon.treasure.domain.exception.MapGameInformationException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.steave.carbon.treasure.domain.fixture.MyGameFixture.createTreasureGameTypeToCoordinates;
import static fr.steave.carbon.treasure.domain.model.Move.Constants.D;
import static fr.steave.carbon.treasure.domain.model.Move.Constants.G;
import static fr.steave.carbon.treasure.domain.model.Orientation.Constants.EAST;
import static fr.steave.carbon.treasure.domain.model.Orientation.Constants.NORTH;
import static fr.steave.carbon.treasure.domain.model.Orientation.Constants.SOUTH;
import static fr.steave.carbon.treasure.domain.model.Orientation.Constants.WEST;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.A;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.M;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.T;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class MyGameTest {

    private static final Map<String, List<Coordinate>> TREASURE_GAME_TYPE_TO_COORDINATES_EMPTY = createTreasureGameTypeToCoordinates();

    @BeforeEach
    void init() {
    }

    @Test
    void givenNullMyMap_constructor_shouldDisplayAnErrorMessage() {
        // WHEN THEN
        final MapGameInformationException mapGameInformationException = assertThrows(MapGameInformationException.class,
                () -> new MyGame(null, null));

        Assertions.assertThat(mapGameInformationException).hasMessage("MyMap information is missing.");
    }

    @Test
    void givenNullTreasureGameTypeToCoordinates_constructor_shouldDisplayAnErrorMessage() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 3);

        // WHEN THEN
        final MapGameInformationException mapGameInformationException = assertThrows(MapGameInformationException.class,
                () -> new MyGame(myMap, null));

        Assertions.assertThat(mapGameInformationException).hasMessage("TreasureGameTypeToCoordinates information is missing.");
    }

    @Test
    void givenNonExistingMountainKey_constructor_shouldDisplayAnErrorMessage() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 3);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();

        // WHEN THEN
        final MapGameInformationException mapGameInformationException = assertThrows(MapGameInformationException.class,
                () -> new MyGame(myMap, treasureGameTypeToCoordinates));

        assertThat(mapGameInformationException).hasMessage("Mountain information is missing.");
    }

    @Test
    void givenNonExistingTreasureKey_constructor_shouldDisplayAnErrorMessage() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 3);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, Collections.emptyList());


        // WHEN THEN
        final MapGameInformationException mapGameInformationException = assertThrows(MapGameInformationException.class,
                () -> new MyGame(myMap, treasureGameTypeToCoordinates));

        assertThat(mapGameInformationException).hasMessage("Treasure information is missing.");
    }

    @Test
    void givenNonExistingAdventurerKey_constructor_shouldDisplayAnErrorMessage() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 3);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, Collections.emptyList());
        treasureGameTypeToCoordinates.put(T, Collections.emptyList());


        // WHEN THEN
        final MapGameInformationException mapGameInformationException = assertThrows(MapGameInformationException.class,
                () -> new MyGame(myMap, treasureGameTypeToCoordinates));

        assertThat(mapGameInformationException).hasMessage("Adventurer information is missing.");
    }

    @Test
    void givenMyMap_constructor_shouldInitializeHorizontalAndVerticalCells() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final MyGame myGame = new MyGame(myMap, TREASURE_GAME_TYPE_TO_COORDINATES_EMPTY);

        // WHEN
        final int horizontalCells = myGame.getHorizontalCells().horizontalCells();
        final int verticalCells = myGame.getVerticalCells().verticalCells();

        // THEN
        assertThat(horizontalCells).isEqualTo(myMap.getHorizontal());
        assertThat(verticalCells).isEqualTo(myMap.getVertical());
    }

    @Test
    void givenMyMap_constructor_shouldDisplayDisplayEmptyGridMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final MyGame myGame = new MyGame(myMap, TREASURE_GAME_TYPE_TO_COORDINATES_EMPTY);

        // WHEN
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\t.\t.\t
                .\t.\t.\t
                .\t.\t.\t
                .\t.\t.\t
                """);
    }

    @Test
    void givenSomethingOtherThanMountainInMKey_constructor_shouldDisplayAnErrorMessage() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Treasure(1, 1, 1)));
        treasureGameTypeToCoordinates.put(T, Collections.emptyList());
        treasureGameTypeToCoordinates.put(A, Collections.emptyList());

        // WHEN THEN
        final MapGameClassException mapGameClassException = assertThrows(MapGameClassException.class,
                () -> new MyGame(myMap, treasureGameTypeToCoordinates));

        assertThat(mapGameClassException).hasMessage("Mountain class information is wrong.");
    }

    @Test
    void givenSomethingOtherThanTreasureInTKey_constructor_shouldDisplayAnErrorMessage() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, Collections.emptyList());
        treasureGameTypeToCoordinates.put(T, List.of(new Mountain(1, 1)));
        treasureGameTypeToCoordinates.put(A, Collections.emptyList());

        // WHEN THEN
        final MapGameClassException mapGameClassException = assertThrows(MapGameClassException.class,
                () -> new MyGame(myMap, treasureGameTypeToCoordinates));

        assertThat(mapGameClassException).hasMessage("Treasure class information is wrong.");
    }

    @Test
    void givenSomethingOtherThanAdventurerInTKey_constructor_shouldDisplayAnErrorMessage() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, Collections.emptyList());
        treasureGameTypeToCoordinates.put(T, Collections.emptyList());
        treasureGameTypeToCoordinates.put(A, List.of(new Mountain(1, 1)));

        // WHEN THEN
        final MapGameClassException mapGameClassException = assertThrows(MapGameClassException.class,
                () -> new MyGame(myMap, treasureGameTypeToCoordinates));

        assertThat(mapGameClassException).hasMessage("Adventurer class information is wrong.");
    }

    @Test
    void givenMountain_getMapGrid_shouldAddElementInGridMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 1)));
        treasureGameTypeToCoordinates.put(T, Collections.emptyList());
        treasureGameTypeToCoordinates.put(A, Collections.emptyList());

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\t.\t.\t
                .\tM\t.\t
                .\t.\t.\t
                .\t.\t.\t
                """);
    }

    @Test
    void givenPositionNotInTheGrid_getMapGrid_shouldNotAddElementInGridMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(10, 10)));
        treasureGameTypeToCoordinates.put(T, List.of(new Treasure(10, 10, 1)));
        treasureGameTypeToCoordinates.put(A, List.of(new Adventurer(10, 10, "John Doe", SOUTH, Collections.emptyList())));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\t.\t.\t
                .\t.\t.\t
                .\t.\t.\t
                .\t.\t.\t
                """);
    }

    @Test
    void givenCoordinates_getMapGrid_shouldDisplayMapWithElements() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 0), new Mountain(2, 1)));
        treasureGameTypeToCoordinates.put(T, List.of(new Treasure(0, 3, 2), new Treasure(1, 3, 3)));
        treasureGameTypeToCoordinates.put(A, List.of(new Adventurer(1, 1, "Lara", SOUTH, List.of(A, A, D, A, D, A, G, G, A))));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tM\t.\t
                .\tA(Lara)\tM\t
                .\t.\t.\t
                T(2)\tT(3)\t.\t
                """);
    }

    @Test
    void givenMovementToSouth_getMapGrid_shouldUpdateTheMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 0), new Mountain(2, 1)));
        treasureGameTypeToCoordinates.put(T, List.of(new Treasure(0, 3, 2), new Treasure(1, 3, 3)));
        treasureGameTypeToCoordinates.put(A, List.of(new Adventurer(1, 1, "Lara", SOUTH, List.of(A))));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        myGame.doAllMovementsOfAdventurers();
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tM\t.\t
                .\t.\tM\t
                .\tA(Lara)\t.\t
                T(2)\tT(3)\t.\t
                """);
    }

    @Test
    void givenOneMovementButThereIsAMountain_getMapGrid_shouldNotUpdateTheMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 0), new Mountain(1, 2)));
        treasureGameTypeToCoordinates.put(T, List.of(new Treasure(0, 3, 2), new Treasure(1, 3, 3)));
        treasureGameTypeToCoordinates.put(A, List.of(new Adventurer(1, 1, "Lara", SOUTH, List.of(A))));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        myGame.doAllMovementsOfAdventurers();
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tM\t.\t
                .\tA(Lara)\t.\t
                .\tM\t.\t
                T(2)\tT(3)\t.\t
                """);
    }

    @Test
    void givenMovementToNorth_getMapGrid_shouldUpdateTheMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(2, 1)));
        treasureGameTypeToCoordinates.put(T, List.of(new Treasure(0, 3, 2), new Treasure(1, 3, 3)));
        treasureGameTypeToCoordinates.put(A, List.of(new Adventurer(1, 1, "Lara", NORTH, List.of(A))));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        myGame.doAllMovementsOfAdventurers();
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tA(Lara)\t.\t
                .\t.\tM\t
                .\t.\t.\t
                T(2)\tT(3)\t.\t
                """);
    }

    @Test
    void givenMovementToEast_getMapGrid_shouldUpdateTheMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 0)));
        treasureGameTypeToCoordinates.put(T, List.of(new Treasure(0, 3, 2), new Treasure(1, 3, 3)));
        treasureGameTypeToCoordinates.put(A, List.of(new Adventurer(1, 1, "Lara", EAST, List.of(A))));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        myGame.doAllMovementsOfAdventurers();
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tM\t.\t
                .\t.\tA(Lara)\t
                .\t.\t.\t
                T(2)\tT(3)\t.\t
                """);
    }

    @Test
    void givenMovementToWest_getMapGrid_shouldUpdateTheMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 0), new Mountain(2, 1)));
        treasureGameTypeToCoordinates.put(T, List.of(new Treasure(0, 3, 2), new Treasure(1, 3, 3)));
        treasureGameTypeToCoordinates.put(A, List.of(new Adventurer(1, 1, "Lara", WEST, List.of(A))));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        myGame.doAllMovementsOfAdventurers();
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tM\t.\t
                A(Lara)\t.\tM\t
                .\t.\t.\t
                T(2)\tT(3)\t.\t
                """);
    }

    @Test
    void givenDoMovementsAndTurnRightOnce_getMapGrid_shouldUpdateTheMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 0), new Mountain(2, 1)));

        final Treasure treasure1 = new Treasure(0, 3, 2);
        final Treasure treasure2 = new Treasure(1, 3, 3);
        treasureGameTypeToCoordinates.put(T, List.of(treasure1, treasure2));

        final Adventurer adventurer = new Adventurer(1, 1, "Lara", SOUTH, List.of(A, A, D));
        treasureGameTypeToCoordinates.put(A, List.of(adventurer));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        assertThat(adventurer.getOrientation()).isEqualTo(Orientation.SOUTH);
        myGame.doAllMovementsOfAdventurers();
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tM\t.\t
                .\t.\tM\t
                .\t.\t.\t
                T(2)\tA(Lara)\t.\t
                """);

        assertThat(adventurer.getPoint()).isEqualTo(1);
        assertThat(adventurer.getOrientation()).isEqualTo(Orientation.WEST);

        assertThat(treasure1.getValue()).isEqualTo(2);
        assertThat(treasure2.getValue()).isEqualTo(2);
    }

    @Test
    void givenMovementToWestAndPickUpTreasure_getMapGrid_shouldUpdateTheMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 0), new Mountain(2, 1)));

        final Treasure treasure1 = new Treasure(0, 1, 2);
        final Treasure treasure2 = new Treasure(1, 3, 3);
        treasureGameTypeToCoordinates.put(T, List.of(treasure1, treasure2));

        final Adventurer adventurer = new Adventurer(1, 1, "Lara", WEST, List.of(A));
        treasureGameTypeToCoordinates.put(A, List.of(adventurer));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        assertThat(treasure1.getValue()).isEqualTo(2);
        assertThat(adventurer.getPoint()).isZero();

        myGame.doAllMovementsOfAdventurers();
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tM\t.\t
                A(Lara)\t.\tM\t
                .\t.\t.\t
                .\tT(3)\t.\t
                """);
        assertThat(adventurer.getPoint()).isEqualTo(1);
        assertThat(treasure1.getValue()).isEqualTo(1);
    }

    @Test
    void givenDoMovementsAndPickUpTreasure_getMapGrid_shouldUpdateTheMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 0)));

        final Treasure treasure = new Treasure(1, 2, 2);
        treasureGameTypeToCoordinates.put(T, List.of(treasure));

        final Adventurer adventurer = new Adventurer(1, 1, "Lara", SOUTH, List.of(A, A));
        treasureGameTypeToCoordinates.put(A, List.of(adventurer));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        assertThat(treasure.getValue()).isEqualTo(2);
        assertThat(adventurer.getPoint()).isZero();

        myGame.doAllMovementsOfAdventurers();
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tM\t.\t
                .\t.\t.\t
                .\tT(1)\t.\t
                .\tA(Lara)\t.\t
                """);
        assertThat(adventurer.getPoint()).isEqualTo(1);
        assertThat(treasure.getValue()).isEqualTo(1);
    }


    @Test
    void givenDoMovements_getMapGrid_shouldUpdateTheMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 0), new Mountain(2, 1)));

        final Treasure treasure1 = new Treasure(0, 3, 2);
        final Treasure treasure2 = new Treasure(1, 3, 3);
        treasureGameTypeToCoordinates.put(T, List.of(treasure1, treasure2));

        final Adventurer adventurer = new Adventurer(1, 1, "Lara", SOUTH, List.of(A, A, D, A));
        treasureGameTypeToCoordinates.put(A, List.of(adventurer));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        myGame.doAllMovementsOfAdventurers();
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tM\t.\t
                .\t.\tM\t
                .\t.\t.\t
                A(Lara)\tT(2)\t.\t
                """);

        assertThat(adventurer.getPoint()).isEqualTo(2);
        assertThat(adventurer.getOrientation()).isEqualTo(Orientation.WEST);

        assertThat(treasure1.getValue()).isEqualTo(1);
        assertThat(treasure2.getValue()).isEqualTo(2);
    }

    @Test
    void givenAllMovements_getMapGrid_shouldUpdateTheMap() {
        // GIVEN
        final MyMap myMap = new MyMap(3, 4);
        final Map<String, List<Coordinate>> treasureGameTypeToCoordinates = new HashMap<>();
        treasureGameTypeToCoordinates.put(M, List.of(new Mountain(1, 0), new Mountain(2, 1)));

        final Treasure treasure1 = new Treasure(0, 3, 2);
        final Treasure treasure2 = new Treasure(1, 3, 3);
        treasureGameTypeToCoordinates.put(T, List.of(treasure1, treasure2));

        final Adventurer adventurer = new Adventurer(1, 1, "Lara", SOUTH, List.of(A, A, D, A, D, A, G, G, A));
        treasureGameTypeToCoordinates.put(A, List.of(adventurer));

        final MyGame myGame = new MyGame(myMap, treasureGameTypeToCoordinates);

        // WHEN
        myGame.doAllMovementsOfAdventurers();
        final String getMapGame = myGame.getMapGrid();

        // THEN
        assertThat(getMapGame).isEqualTo("""
                .\tM\t.\t
                .\t.\tM\t
                .\t.\t.\t
                A(Lara)\tT(2)\t.\t
                """);

        assertThat(adventurer.getPoint()).isEqualTo(3);
        assertThat(adventurer.getOrientation()).isEqualTo(Orientation.SOUTH);

        assertThat(treasure1.getValue()).isZero();
        assertThat(treasure2.getValue()).isEqualTo(2);
    }
}
