package fr.steave.carbon.treasure.domain.service;

import fr.steave.carbon.treasure.domain.model.Adventurer;
import fr.steave.carbon.treasure.domain.model.Coordinate;
import fr.steave.carbon.treasure.domain.model.Mountain;
import fr.steave.carbon.treasure.domain.model.MyMap;
import fr.steave.carbon.treasure.domain.model.TextFileToUpload;
import fr.steave.carbon.treasure.domain.model.Treasure;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static fr.steave.carbon.treasure.domain.model.Orientation.Constants.SOUTH;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.A;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.C;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.M;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.T;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class MapGameFileServiceImplTest {
    private static final String VALID_FILE_CONTENT_TYPE = "text/plain";

    @InjectMocks
    private MapGameFileServiceImpl mapGameFileServiceImpl;

    @Test
    void initializeMap_shouldInitializeKeysCorrectly() {
        // WHEN
        final TextFileToUpload textFileToUpload = new TextFileToUpload(VALID_FILE_CONTENT_TYPE, "".getBytes());
        final Map<String, List<Coordinate>> coordinatesMap = this.mapGameFileServiceImpl.filterAndParseCoordinates(textFileToUpload);

        // THEN
        assertThat(coordinatesMap).containsOnlyKeys(C, M, T, A);
        assertThat(coordinatesMap.get(A)).isEmpty();
        assertThat(coordinatesMap.get(C)).isEmpty();
        assertThat(coordinatesMap.get(M)).isEmpty();
        assertThat(coordinatesMap.get(T)).isEmpty();
    }

    @ParameterizedTest
    @ValueSource(strings = {
            "C - ",
            "C - 3",
            "C - 3 - 4 - 5",
            "C-3-4",
            "M - ",
            "M - 3",
            "M - 3 - 4 - 5",
            "M-3-4",
            "T - ",
            "T - 3",
            "T - 3 - 4",
            "T - 3 - 4 - 5 - 6",
            "T-3-4-5",
            "A - ",
            "A - 1",
            "A - Indiana",
            "A - Indiana - 1",
            "A - Indiana - 1 - 1",
            "A - Indiana - 1 - 1 - S",
            "A - Indiana - 1 - 1 - 1",
            "A - Indiana - 1 - 1 - S - ",
    })
    void givenInvalidCoordinates_filterAndParseCoordinates_shouldDisplayAnErrorMessage(String coordinate) {
        // GIVEN
        final byte[] invalidMapCoordinateBytes = coordinate.getBytes();
        final TextFileToUpload textFileToUpload = new TextFileToUpload(VALID_FILE_CONTENT_TYPE, invalidMapCoordinateBytes);

        // WHEN THEN
        final IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> this.mapGameFileServiceImpl.filterAndParseCoordinates(textFileToUpload));

        assertThat(exception).hasMessage("Invalid file content: " + coordinate);
    }

    @Test
    void givenMapGameInformation_filterAndParseCoordinates_shouldAddMapGameInTheListOfCoordinates() {
        // GIVEN
        final String validCoordinate = "C - 3 - 4";
        final TextFileToUpload textFileToUpload = new TextFileToUpload(VALID_FILE_CONTENT_TYPE, validCoordinate.getBytes());

        // WHEN
        final Map<String, List<Coordinate>> typeToListOfCoordinatesMap = this.mapGameFileServiceImpl.filterAndParseCoordinates(textFileToUpload);

        // THEN
        final ArrayList<Coordinate> coordinates = new ArrayList<>(List.of(new MyMap(3, 4)));
        assertThat(typeToListOfCoordinatesMap).containsEntry(A, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(C, coordinates);
        assertThat(typeToListOfCoordinatesMap).containsEntry(M, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(T, Collections.emptyList());
    }

    @Test
    void givenMountainInformation_filterAndParseCoordinates_shouldAddMountainInTheListOfCoordinates() {
        // GIVEN
        final String validCoordinate = "M - 1 - 1";
        final TextFileToUpload textFileToUpload = new TextFileToUpload(VALID_FILE_CONTENT_TYPE, validCoordinate.getBytes());

        // WHEN
        final Map<String, List<Coordinate>> typeToListOfCoordinatesMap = this.mapGameFileServiceImpl.filterAndParseCoordinates(textFileToUpload);

        // THEN
        final ArrayList<Coordinate> coordinates = new ArrayList<>(List.of(new Mountain(1, 1)));
        assertThat(typeToListOfCoordinatesMap).containsEntry(A, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(C, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(M, coordinates);
        assertThat(typeToListOfCoordinatesMap).containsEntry(T, Collections.emptyList());
    }

    @Test
    void givenTreasureInformation_filterAndParseCoordinates_shouldAddTreasureInTheListOfCoordinates() {
        // GIVEN
        final String validCoordinate = "T - 0 - 3 - 2";
        final TextFileToUpload textFileToUpload = new TextFileToUpload(VALID_FILE_CONTENT_TYPE, validCoordinate.getBytes());

        // WHEN
        final Map<String, List<Coordinate>> typeToListOfCoordinatesMap = this.mapGameFileServiceImpl.filterAndParseCoordinates(textFileToUpload);

        // THEN
        final ArrayList<Coordinate> coordinates = new ArrayList<>(List.of(new Treasure(0, 3, 2)));
        assertThat(typeToListOfCoordinatesMap).containsEntry(A, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(C, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(M, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(T, coordinates);
    }

    @Test
    void givenAdventurer_filterAndParseCoordinates_shouldAddAdventurerInListOfCoordinates() {
        // GIVEN
        final String validCoordinate = "A - Indiana - 1 - 1 - S - A";
        final TextFileToUpload textFileToUpload = new TextFileToUpload(VALID_FILE_CONTENT_TYPE, validCoordinate.getBytes());

        // WHEN
        final Map<String, List<Coordinate>> typeToListOfCoordinatesMap = this.mapGameFileServiceImpl.filterAndParseCoordinates(textFileToUpload);

        // THEN
        final ArrayList<Coordinate> coordinates = new ArrayList<>(List.of(new Adventurer(1, 1, "Indiana", SOUTH, List.of("A"))));
        assertThat(typeToListOfCoordinatesMap).containsEntry(A, coordinates);
        assertThat(typeToListOfCoordinatesMap).containsEntry(C, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(M, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(T, Collections.emptyList());
    }

    @Test
    void givenCommentedLine_filterAndParseCoordinates_shouldAddNothingInListOfCoordinates() {
        // GIVEN
        final String validCoordinate = "# C";
        final TextFileToUpload textFileToUpload = new TextFileToUpload(VALID_FILE_CONTENT_TYPE, validCoordinate.getBytes());

        // WHEN
        final Map<String, List<Coordinate>> typeToListOfCoordinatesMap = this.mapGameFileServiceImpl.filterAndParseCoordinates(textFileToUpload);

        // THEN
        assertThat(typeToListOfCoordinatesMap).containsEntry(A, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(C, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(M, Collections.emptyList());
        assertThat(typeToListOfCoordinatesMap).containsEntry(T, Collections.emptyList());
    }
}
