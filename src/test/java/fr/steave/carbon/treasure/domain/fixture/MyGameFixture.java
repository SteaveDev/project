package fr.steave.carbon.treasure.domain.fixture;

import fr.steave.carbon.treasure.domain.model.Coordinate;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.A;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.C;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.M;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.T;

public class MyGameFixture {

    public static Map<String, List<Coordinate>> createTreasureGameTypeToCoordinates() {
        final Map<String, List<Coordinate>> expectedResults = new HashMap<>();
        expectedResults.put(M, Collections.emptyList());
        expectedResults.put(T, Collections.emptyList());
        expectedResults.put(A, Collections.emptyList());

        return expectedResults;
    }
}
