package fr.steave.carbon.treasure.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class MyMapTest {

    @Test
    void givenValidInformation_whenCreateMapGame_thenMapGameIsCreated() {
        // GIVEN
        final String[] info = {"C", "5", "6"};

        // WHEN
        final MyMap myMap = MyMap.createMapGame(info);

        // THEN
        assertEquals(5, myMap.getHorizontal());
        assertEquals(6, myMap.getVertical());
    }

    @Test
    void givenMyMap_whenToString_thenCorrectStringIsReturned() {
        // GIVEN
        final MyMap myMap = new MyMap(5, 6);

        // WHEN THEN
        assertEquals(".", myMap.toString());
    }

    @Test
    void givenTwoMaps_whenEqualsAndHashCode_thenCorrectComparison() {
        // GIVEN
        final MyMap myMap1 = new MyMap(5, 6);
        final MyMap myMap2 = new MyMap(5, 6);
        final MyMap myMap3 = new MyMap(7, 8);

        // WHEN THEN
        assertEquals(myMap1, myMap2);
        assertNotEquals(myMap1, myMap3);
        assertEquals(myMap1.hashCode(), myMap2.hashCode());
        assertNotEquals(myMap1.hashCode(), myMap3.hashCode());
    }
}
