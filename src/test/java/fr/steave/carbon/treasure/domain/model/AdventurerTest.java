package fr.steave.carbon.treasure.domain.model;

import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static fr.steave.carbon.treasure.domain.model.Move.Constants.A;
import static fr.steave.carbon.treasure.domain.model.Move.Constants.D;
import static fr.steave.carbon.treasure.domain.model.Move.Constants.G;
import static fr.steave.carbon.treasure.domain.model.Orientation.Constants.EAST;
import static fr.steave.carbon.treasure.domain.model.Orientation.Constants.NORTH;
import static fr.steave.carbon.treasure.domain.model.Orientation.Constants.SOUTH;
import static fr.steave.carbon.treasure.domain.model.Orientation.Constants.WEST;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class AdventurerTest {

    @Test
    void givenValidInformation_whenCreateAdventurer_thenAdventurerIsCreated() {
        // GIVEN
        final String[] information = {A, "John", "1", "2", NORTH, "ADG"};

        // WHEN
        final var adventurer = Adventurer.createAdventurer(information);

        // THEN
        assertEquals("John", adventurer.getName().name());
        assertEquals(1, adventurer.getHorizontal());
        assertEquals(2, adventurer.getVertical());
        assertEquals(Orientation.NORTH, adventurer.getOrientation());
        assertEquals(List.of(A, D, G), adventurer.getMovements());
    }

    @Test
    void givenInvalidOrientation_whenBuildOrientation_thenThrowsIllegalArgumentException() {
        // GIVEN
        final Adventurer adventurer = new Adventurer(1, 2, "John", NORTH, List.of(A, D, G));

        // WHEN THEN
        final IllegalArgumentException illegalArgumentException = assertThrows(IllegalArgumentException.class,
                () -> adventurer.buildOrientation("X"));

        assertThat(illegalArgumentException).hasMessage("Orientation invalide: X");
    }

    @Test
    void givenOrientationString_whenBuildOrientation_thenCorrectOrientationIsReturned() {
        // GIVEN
        final var adventurer = new Adventurer(1, 2, "John", NORTH, List.of(A, D, G));

        // WHEN THEN
        assertEquals(Orientation.NORTH, adventurer.buildOrientation(NORTH));
        assertEquals(Orientation.SOUTH, adventurer.buildOrientation(SOUTH));
        assertEquals(Orientation.WEST, adventurer.buildOrientation(WEST));
        assertEquals(Orientation.EAST, adventurer.buildOrientation(EAST));
    }

    @Test
    void givenAdventurer_whenToString_thenCorrectStringIsReturned() {
        // GIVEN
        final var adventurer = new Adventurer(1, 2, "John", NORTH, List.of(A, D, G));

        // WHEN THEN
        assertEquals("A(John)", adventurer.toString());
    }

    @Test
    void givenTwoAdventurers_whenEqualsAndHashCode_thenCorrectComparison() {
        // GIVEN
        final var adventurer1 = new Adventurer(1, 2, "John", NORTH, List.of(A, D, G));
        final var adventurer2 = new Adventurer(1, 2, "John", NORTH, List.of(A, D, G));
        final var adventurer3 = new Adventurer(1, 2, "Jane", NORTH, List.of(A, D, G));

        // WHEN THEN
        assertEquals(adventurer1, adventurer2);
        assertNotEquals(adventurer1, adventurer3);
        assertEquals(adventurer1.hashCode(), adventurer2.hashCode());
        assertNotEquals(adventurer1.hashCode(), adventurer3.hashCode());
    }

    @Test
    void givenTurnLeftOnce_whenOrientationIsUpdated_thenCorrectOrientation() {
        // GIVEN
        final var adventurer = new Adventurer(1, 2, "John", NORTH, Collections.emptyList());

        // WHEN
        adventurer.turnLeft();

        // THEN
        assertEquals(Orientation.WEST, adventurer.getOrientation());
    }

    @Test
    void givenTurnRightOnce_whenOrientationIsUpdated_thenCorrectOrientation() {
        // GIVEN
        final var adventurer = new Adventurer(1, 2, "John", NORTH, Collections.emptyList());

        // WHEN
        adventurer.turnRight();

        // THEN
        assertEquals(Orientation.EAST, adventurer.getOrientation());
    }

    @Test
    void givenTurnLeftTwice_whenOrientationIsUpdated_thenCorrectOrientation() {
        // GIVEN
        final var adventurer = new Adventurer(1, 2, "John", NORTH, Collections.emptyList());

        // WHEN
        adventurer.turnLeft();
        adventurer.turnLeft();

        // THEN
        assertEquals(Orientation.SOUTH, adventurer.getOrientation());
    }

    @Test
    void givenTurnRightTwice_whenOrientationIsUpdated_thenCorrectOrientation() {
        // GIVEN
        final var adventurer = new Adventurer(1, 2, "John", NORTH, Collections.emptyList());

        // WHEN
        adventurer.turnRight();
        adventurer.turnRight();

        // THEN
        assertEquals(Orientation.SOUTH, adventurer.getOrientation());
    }

    @Test
    void givenTurnLeftThreeTimes_whenOrientationIsUpdated_thenCorrectOrientation() {
        // GIVEN
        final var adventurer = new Adventurer(1, 2, "John", NORTH, Collections.emptyList());

        // WHEN
        adventurer.turnLeft();
        adventurer.turnLeft();
        adventurer.turnLeft();

        // THEN
        assertEquals(Orientation.EAST, adventurer.getOrientation());
    }

    @Test
    void givenTurnRightThreeTimes_whenOrientationIsUpdated_thenCorrectOrientation() {
        // GIVEN
        final var adventurer = new Adventurer(1, 2, "John", NORTH, Collections.emptyList());

        // WHEN
        adventurer.turnRight();
        adventurer.turnRight();
        adventurer.turnRight();

        // THEN
        assertEquals(Orientation.WEST, adventurer.getOrientation());
    }

    @Test
    void givenTurnLeftFourTimes_whenOrientationIsUpdated_thenCorrectOrientation() {
        // GIVEN
        final var adventurer = new Adventurer(1, 2, "John", NORTH, Collections.emptyList());

        // WHEN
        adventurer.turnLeft();
        adventurer.turnLeft();
        adventurer.turnLeft();
        adventurer.turnLeft();

        // THEN
        assertEquals(Orientation.NORTH, adventurer.getOrientation());
    }

    @Test
    void givenTurnRightFourTimes_whenOrientationIsUpdated_thenCorrectOrientation() {
        // GIVEN
        final var adventurer = new Adventurer(1, 2, "John", NORTH, Collections.emptyList());

        // WHEN
        adventurer.turnRight();
        adventurer.turnRight();
        adventurer.turnRight();
        adventurer.turnRight();

        // THEN
        assertEquals(Orientation.NORTH, adventurer.getOrientation());
    }

}
