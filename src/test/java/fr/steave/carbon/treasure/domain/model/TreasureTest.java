package fr.steave.carbon.treasure.domain.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class TreasureTest {

    @Test
    void givenValidInformation_whenCreateTreasure_thenTreasureIsCreated() {
        // GIVEN
        final String[] info = {"T", "3", "4", "5"};

        // WHEN
        final Treasure treasure = Treasure.createTreasure(info);

        // THEN
        assertEquals(3, treasure.getHorizontal());
        assertEquals(4, treasure.getVertical());
        assertEquals(5, treasure.getValue());
    }

    @Test
    void givenTreasure_whenToString_thenCorrectStringIsReturned() {
        // GIVEN
        final Treasure treasure = new Treasure(3, 4, 5);

        // WHEN THEN
        assertEquals("T(5)", treasure.toString());
    }

    @Test
    void givenTreasure_whenVisited_thenValueDecreases() {
        // GIVEN
        final Treasure treasure = new Treasure(3, 4, 5);

        // WHEN
        treasure.visited();

        // THEN
        assertEquals(4, treasure.getValue());
    }

    @Test
    void givenTreasureWithZeroValue_whenVisited_thenValueRemainsZero() {
        // GIVEN
        final Treasure treasure = new Treasure(3, 4, 0);

        // WHEN
        treasure.visited();

        // THEN
        assertEquals(0, treasure.getValue());
    }

    @Test
    void givenTwoTreasures_whenEqualsAndHashCode_thenCorrectComparison() {
        // GIVEN
        final Treasure treasure1 = new Treasure(3, 4, 5);
        final Treasure treasure2 = new Treasure(3, 4, 5);
        final Treasure treasure3 = new Treasure(3, 4, 6);

        // WHEN THEN
        assertEquals(treasure1, treasure2);
        assertNotEquals(treasure1, treasure3);
        assertEquals(treasure1.hashCode(), treasure2.hashCode());
        assertNotEquals(treasure1.hashCode(), treasure3.hashCode());
    }
}
