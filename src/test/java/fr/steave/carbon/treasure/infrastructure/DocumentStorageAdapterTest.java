package fr.steave.carbon.treasure.infrastructure;

import fr.steave.carbon.treasure.domain.exception.DocumentFileServerException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.MockedStatic;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class DocumentStorageAdapterTest {

    @InjectMocks
    private DocumentStorageAdapter documentStorageAdapter;

    @Test
    void givenCorrectFileProperties_uploadTextFile_shouldCreateFile() {
        // GIVEN
        String fileName = "testFile";
        String fileContent = "This is a test file content";
        Path filePath = Paths.get(fileName + ".txt");

        try (MockedStatic<Files> mockedFiles = mockStatic(Files.class)) {
            mockedFiles.when(() -> Files.writeString(filePath, fileContent)).thenReturn(filePath);

            // WHEN
            documentStorageAdapter.uploadTextFile(fileName, fileContent);

            // THEN
            mockedFiles.verify(() -> Files.writeString(filePath, fileContent), times(1));
        }
    }

    @Test
    void givenIncorrectFileProperties_uploadTextFile_shouldFailTheCreationOfTheFile() {
        // GIVEN
        final String fileName = "testFile";
        final String fileContent = "This is a test file content";
        final Path filePath = Paths.get(fileName + ".txt");

        try (MockedStatic<Files> mockedFiles = mockStatic(Files.class)) {
            mockedFiles.when(() -> Files.writeString(filePath, fileContent)).thenThrow(IOException.class);

            // WHEN THEN
            assertThrows(DocumentFileServerException.class,
                    () -> documentStorageAdapter.uploadTextFile(fileName, fileContent));

            mockedFiles.verify(() -> Files.writeString(filePath, fileContent), times(1));
        }
    }
}
