package fr.steave.carbon.error;

public class NumberValueTooLowException extends AssertionException {

    private NumberValueTooLowException(final NumberValueTooLowBuilder builder) {
        super(builder.message());
    }

    public static NumberValueTooLowBuilder builder() {
        return new NumberValueTooLowBuilder();
    }

    static class NumberValueTooLowBuilder {

        private double value;
        private double minValue;
        private String field;

        private NumberValueTooLowBuilder() {
        }

        NumberValueTooLowBuilder field(final String field) {
            this.field = field;

            return this;
        }

        NumberValueTooLowBuilder value(final double value) {
            this.value = value;

            return this;
        }

        NumberValueTooLowBuilder minValue(final double minValue) {
            this.minValue = minValue;

            return this;
        }

        private String message() {
            return "The value in field \"" +
                    field +
                    "\" must be at least " +
                    minValue +
                    " but was " +
                    value;
        }

        public NumberValueTooLowException build() {
            return new NumberValueTooLowException(this);
        }
    }
}
