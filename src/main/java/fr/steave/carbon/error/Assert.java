package fr.steave.carbon.error;

public class Assert {

    private final String context;
    private final Class<?> assertedClass;

    private Assert(final Class<?> assertedClass, final String context) {
        this.context = context;
        this.assertedClass = assertedClass;
    }

    public static Assert builder(final Class<?> assertedClass) {
        return new Assert(assertedClass, assertedClass.getSimpleName());
    }

    public Assert notNull(final Object input, final String field) {
        if (input == null) {
            throw MissingMandatoryValueException.forNullValue(this.buildMessage(field));
        }
        return this;
    }

    public Assert notBlank(final String input, final String field) {
        if (input.isBlank()) {
            throw MissingMandatoryValueException.forBlankValue(this.buildMessage(field));
        }
        return this;
    }

    public Assert minValue(final double input, final double minValue, final String field) {
        if (input < minValue)
            throw NumberValueTooLowException.builder().field(this.buildMessage(field)).value(input).minValue(minValue).build();
        return this;
    }

    private String buildMessage(final String field) {
        return String.format("%s's %s", this.context, field);
    }

}
