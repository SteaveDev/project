package fr.steave.carbon.error;

public class AssertionException extends IllegalArgumentException {

    public AssertionException(String message) {
        super(message);
    }

}
