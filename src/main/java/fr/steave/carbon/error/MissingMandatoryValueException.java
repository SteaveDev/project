package fr.steave.carbon.error;

public class MissingMandatoryValueException extends AssertionException {

    private MissingMandatoryValueException(String message) {
        super(message);
    }

    public static MissingMandatoryValueException forBlankValue(String field) {
        return new MissingMandatoryValueException(defaultMessage(field, "blank"));
    }

    public static MissingMandatoryValueException forNullValue(String field) {
        return new MissingMandatoryValueException(defaultMessage(field, "null"));
    }

    private static String defaultMessage(String field, String reason) {
        return "The field \"" +
                field +
                "\" is mandatory and wasn't set" +
                " (" +
                reason +
                ")";
    }
}
