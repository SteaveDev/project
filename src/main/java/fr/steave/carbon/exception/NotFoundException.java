package fr.steave.carbon.exception;

import static java.lang.String.format;

public class NotFoundException extends RuntimeException {

    private final String resourceName;

    private final Long id;

    private final String label;

    public NotFoundException(final String resourceName, final Long id) {
        super(format("Id %s of %s not found.", id, resourceName));
        this.id = id;
        this.resourceName = resourceName;
        this.label = null;
    }

    public NotFoundException(final String resourceName, final String label) {
        super(format("%s %s not found.", resourceName, label));
        this.label = label;
        this.resourceName = resourceName;
        this.id = null;
    }

    public String getResourceName() {
        return resourceName;
    }

    public Long getId() {
        return id;
    }

    public String getLabel() {
        return label;
    }
}
