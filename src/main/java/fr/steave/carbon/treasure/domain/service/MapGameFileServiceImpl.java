package fr.steave.carbon.treasure.domain.service;

import fr.steave.carbon.treasure.domain.api.MapGameFileService;
import fr.steave.carbon.treasure.domain.model.Adventurer;
import fr.steave.carbon.treasure.domain.model.Coordinate;
import fr.steave.carbon.treasure.domain.model.Mountain;
import fr.steave.carbon.treasure.domain.model.MyMap;
import fr.steave.carbon.treasure.domain.model.TextFileToUpload;
import fr.steave.carbon.treasure.domain.model.Treasure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.A;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.C;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.M;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.T;

public class MapGameFileServiceImpl implements MapGameFileService {

    private static final String COMMENTARY_REGEX = "^#.*$";
    private static final Pattern VALID_LINE_PATTERN = Pattern.compile(
            String.join("|", MyMap.MAP_GAME_REGEX, Mountain.MOUNTAIN_REGEX, Treasure.TREASURE_REGEX, Adventurer.ADVENTURER_REGEX, COMMENTARY_REGEX)
    );

    public MapGameFileServiceImpl() {
    }

    @Override
    public Map<String, List<Coordinate>> filterAndParseCoordinates(final TextFileToUpload textFileToUpload) {
        final Map<String, List<Coordinate>> treasureGameTypeToListOfCoordinatesMap = this.initializeMap();

        final boolean isTextFileBlank = !textFileToUpload.getFileContent().isBlank();
        if (isTextFileBlank) {
            final String[] allCoordinates = textFileToUpload.getFileContent().split("\\n+");

            for (final String coordinate : allCoordinates) {
                if (this.isCommentary(coordinate))
                    continue;

                final Matcher matcher = VALID_LINE_PATTERN.matcher(coordinate);
                final boolean isCoordinateValueValid = matcher.matches();
                if (!isCoordinateValueValid) {
                    throw new IllegalArgumentException("Invalid file content: " + coordinate);
                }

                final String[] coordinateInformation = coordinate.split(" - ");
                final Coordinate coordinateToSave = this.createCoordinateFromInformation(coordinateInformation);

                final String treasureGameTypeKey = coordinateInformation[0];
                treasureGameTypeToListOfCoordinatesMap.get(treasureGameTypeKey).add(coordinateToSave);

            }
        }

        return treasureGameTypeToListOfCoordinatesMap;
    }

    private Map<String, List<Coordinate>> initializeMap() {
        final Map<String, List<Coordinate>> treasureGameTypeToListOfCoordinatesMap = new HashMap<>();
        treasureGameTypeToListOfCoordinatesMap.put(C, new ArrayList<>());
        treasureGameTypeToListOfCoordinatesMap.put(M, new ArrayList<>());
        treasureGameTypeToListOfCoordinatesMap.put(T, new ArrayList<>());
        treasureGameTypeToListOfCoordinatesMap.put(A, new ArrayList<>());

        return treasureGameTypeToListOfCoordinatesMap;
    }

    private boolean isCommentary(final String line) {
        return line.matches(COMMENTARY_REGEX);
    }

    private Coordinate createCoordinateFromInformation(final String[] coordinateInformation) {
        final String treasureGameType = coordinateInformation[0];

        return switch (treasureGameType) {
            case C -> MyMap.createMapGame(coordinateInformation);
            case M -> Mountain.createMountain(coordinateInformation);
            case T -> Treasure.createTreasure(coordinateInformation);
            case A -> Adventurer.createAdventurer(coordinateInformation);
            default -> throw new IllegalArgumentException("Invalid coordinate type: " + treasureGameType);
        };
    }
}
