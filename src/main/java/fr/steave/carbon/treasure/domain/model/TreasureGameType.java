package fr.steave.carbon.treasure.domain.model;

public enum TreasureGameType {
    A,
    T,
    M,
    C;

    public static class Constants {
        public static final String A = "A";
        public static final String T = "T";
        public static final String M = "M";
        public static final String C = "C";
    }
}
