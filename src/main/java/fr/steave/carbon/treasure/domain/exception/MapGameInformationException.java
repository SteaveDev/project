package fr.steave.carbon.treasure.domain.exception;

import static java.lang.String.format;

public class MapGameInformationException extends RuntimeException {

    public MapGameInformationException(final String information) {
        super(format("%s information is missing.", information));
    }

}
