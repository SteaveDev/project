package fr.steave.carbon.treasure.domain.exception;

import static java.lang.String.format;

public class DocumentFileServerException extends RuntimeException {

    public DocumentFileServerException(final String filePath) {
        super(format("Failed to write file: %s", filePath));
    }

}
