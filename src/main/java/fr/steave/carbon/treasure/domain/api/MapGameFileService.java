package fr.steave.carbon.treasure.domain.api;

import fr.steave.carbon.treasure.domain.model.Coordinate;
import fr.steave.carbon.treasure.domain.model.TextFileToUpload;

import java.util.List;
import java.util.Map;

public interface MapGameFileService {

    Map<String, List<Coordinate>> filterAndParseCoordinates(final TextFileToUpload textFileToUpload);

}
