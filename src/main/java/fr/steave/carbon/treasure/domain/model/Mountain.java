package fr.steave.carbon.treasure.domain.model;

public class Mountain extends Coordinate {
    public static final String MOUNTAIN_REGEX = "^M - \\d+ - \\d+$";

    public Mountain(final int horizontal, final int vertical) {
        super(horizontal, vertical);
    }

    public static Mountain createMountain(String[] information) {
        int horizontal = Integer.parseInt(information[1]);
        int vertical = Integer.parseInt(information[2]);

        return new Mountain(horizontal, vertical);
    }

    @Override
    public String toString() {
        return "M";
    }

    @Override
    public boolean equals(final Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
