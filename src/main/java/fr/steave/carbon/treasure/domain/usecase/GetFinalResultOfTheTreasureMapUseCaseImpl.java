package fr.steave.carbon.treasure.domain.usecase;

import fr.steave.carbon.treasure.application.GetFinalResultOfTheTreasureMapUseCase;
import fr.steave.carbon.treasure.domain.api.MapGameFileService;
import fr.steave.carbon.treasure.domain.model.Coordinate;
import fr.steave.carbon.treasure.domain.model.MyGame;
import fr.steave.carbon.treasure.domain.model.MyMap;
import fr.steave.carbon.treasure.domain.model.TextFileToUpload;
import fr.steave.carbon.treasure.domain.spi.DocumentStorageSpi;

import java.util.List;
import java.util.Map;

public class GetFinalResultOfTheTreasureMapUseCaseImpl implements GetFinalResultOfTheTreasureMapUseCase {

    private final MapGameFileService mapGameFileService;
    private final DocumentStorageSpi documentStorageSpi;

    public GetFinalResultOfTheTreasureMapUseCaseImpl(
            final MapGameFileService mapGameFileService,
            final DocumentStorageSpi documentStorageSpi
    ) {
        this.mapGameFileService = mapGameFileService;
        this.documentStorageSpi = documentStorageSpi;
    }

    @Override
    public void execute(final TextFileToUpload textFileToUpload) {
        final Map<String, List<Coordinate>> coordinatesMap = this.mapGameFileService.filterAndParseCoordinates(textFileToUpload);

        final List<Coordinate> map = coordinatesMap.get("C");
        final MyMap myMap = (MyMap) map.get(0);

        final MyGame myGame = new MyGame(myMap, coordinatesMap);
        myGame.doAllMovementsOfAdventurers();

        this.documentStorageSpi.uploadTextFile("result_file", myGame.getMapGrid());
    }
}
