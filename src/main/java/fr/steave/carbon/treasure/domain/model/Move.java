package fr.steave.carbon.treasure.domain.model;

public enum Move {
    A,
    G,
    D;

    public static class Constants {
        public static final String A = "A";
        public static final String G = "G";
        public static final String D = "D";
    }
}
