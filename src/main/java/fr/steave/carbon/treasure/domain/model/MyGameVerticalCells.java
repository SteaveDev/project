package fr.steave.carbon.treasure.domain.model;

import fr.steave.carbon.error.Assert;

public record MyGameVerticalCells( int verticalCells) {

    public MyGameVerticalCells {
        Assert.builder(this.getClass())
                .minValue(verticalCells, 0,"verticalCells");
    }

}
