package fr.steave.carbon.treasure.domain.model;

import java.nio.charset.StandardCharsets;

public class TextFileToUpload {
    private String fileContentType;
    private String fileContent;

    public TextFileToUpload(
            final String fileContentType,
            final byte[] fileContent
    ) {
        this.setFileContentType(fileContentType);
        this.setFileContent(fileContent);
    }

    private static boolean isTextFile(String fileContentType) {
        return fileContentType != null && fileContentType.startsWith("text/");
    }

    public String getFileContentType() {
        return fileContentType;
    }

    private void setFileContentType(final String fileContentType) {
        if (!isTextFile(fileContentType)) {
            throw new IllegalArgumentException("Le fichier n'est pas un fichier texte.");
        }
        this.fileContentType = fileContentType;
    }

    public String getFileContent() {
        return this.fileContent;
    }

    private void setFileContent(final byte[] fileContent) {
        this.fileContent = new String(fileContent, StandardCharsets.UTF_8);
    }
}
