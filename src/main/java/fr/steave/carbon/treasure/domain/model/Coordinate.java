package fr.steave.carbon.treasure.domain.model;

import fr.steave.carbon.error.Assert;

import java.util.Objects;

public abstract class Coordinate {
    private int horizontal;
    private int vertical;

    protected Coordinate(final int horizontal, final int vertical) {
        this.setHorizontal(horizontal);
        this.setVertical(vertical);
    }

    protected int getHorizontal() {
        return horizontal;
    }

    protected void setHorizontal(final int horizontal) {
        Assert.builder(this.getClass())
                .minValue(horizontal, 0, "horizontal");

        this.horizontal = horizontal;
    }

    protected int getVertical() {
        return vertical;
    }

    protected void setVertical(final int vertical) {
        Assert.builder(this.getClass())
                .minValue(vertical, 0, "vertical");

        this.vertical = vertical;
    }

    public boolean isWithinBounds(final int horizontal, final int vertical) {
        final int maxIndexOfHorizontalCells = horizontal - 1;
        final int maxIndexOfVerticalCells = vertical - 1;

        return horizontal >= 0 &&
                vertical >= 0 &&
                this.horizontal <= maxIndexOfHorizontalCells &&
                this.vertical <= maxIndexOfVerticalCells;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof final Coordinate that)) return false;
        return getHorizontal() == that.getHorizontal() && getVertical() == that.getVertical();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHorizontal(), getVertical());
    }
}
