package fr.steave.carbon.treasure.domain.model;

public enum Orientation {
    NORTH,
    SOUTH,
    WEST,
    EAST;

    public static class Constants {
        public static final String NORTH = "N";
        public static final String SOUTH = "S";
        public static final String WEST = "W";
        public static final String EAST = "E";
    }
}
