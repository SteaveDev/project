package fr.steave.carbon.treasure.domain.spi;

public interface DocumentStorageSpi {

    void uploadTextFile(String fileName, String fileContent);

}
