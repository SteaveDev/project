package fr.steave.carbon.treasure.domain.model;

import fr.steave.carbon.error.Assert;

public record AdventurerName(String name) {

    public AdventurerName {
        Assert.builder(this.getClass())
                .notNull(name, "name")
                .notBlank(name, "name");
    }

}
