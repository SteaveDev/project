package fr.steave.carbon.treasure.domain.model;

import fr.steave.carbon.treasure.domain.exception.MapGameClassException;
import fr.steave.carbon.treasure.domain.exception.MapGameInformationException;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static fr.steave.carbon.treasure.domain.model.Move.Constants.D;
import static fr.steave.carbon.treasure.domain.model.Move.Constants.G;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.A;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.M;
import static fr.steave.carbon.treasure.domain.model.TreasureGameType.Constants.T;

public class MyGame {
    private final MyGameHorizontalCells horizontalCells;
    private final MyGameVerticalCells verticalCells;
    private List<Coordinate> mountains;
    private List<Coordinate> treasures;
    private List<Coordinate> adventurers;
    private Coordinate[][] mapGrid;

    public MyGame(final MyMap myMap,
                  final Map<String, List<Coordinate>> treasureGameTypeToCoordinates) {
        this.checkIfCoordinatesContainsRightValues(myMap, treasureGameTypeToCoordinates);
        this.fillInAllTheRightInformationInList(treasureGameTypeToCoordinates);

        this.horizontalCells = new MyGameHorizontalCells(myMap.getHorizontal());
        this.verticalCells = new MyGameVerticalCells(myMap.getVertical());

        this.initializeMapGrid();

        this.validateCoordinatesContentInformation();
        this.placeCoordinatesOnMap();
    }

    public MyGameHorizontalCells getHorizontalCells() {
        return horizontalCells;
    }

    public MyGameVerticalCells getVerticalCells() {
        return verticalCells;
    }

    public String getMapGrid() {
        final StringBuilder mapRepresentation = new StringBuilder();

        for (int y = 0; y < this.getVerticalCells().verticalCells(); y++) {
            for (int x = 0; x < this.getHorizontalCells().horizontalCells(); x++) {
                if (!this.isAnAdventurerInThisIndex(x, y, mapRepresentation)) {
                    mapRepresentation.append(this.mapGrid[x][y]).append("\t");
                }
            }
            mapRepresentation.append("\n");
        }

        return mapRepresentation.toString();
    }

    private boolean isAnAdventurerInThisIndex(final int x, final int y, final StringBuilder mapRepresentation) {
        boolean isAnAdventurerInThisIndex = false;
        for (final Coordinate adventurer : adventurers) {

            final boolean isAdventurerFound = adventurer.getHorizontal() == x && adventurer.getVertical() == y;
            if (isAdventurerFound) {
                mapRepresentation.append(adventurer).append("\t");
                isAnAdventurerInThisIndex = true;
            }
        }
        return isAnAdventurerInThisIndex;
    }

    public void doAllMovementsOfAdventurers() {
        this.adventurers.stream()
                .filter(Adventurer.class::isInstance)
                .map(Adventurer.class::cast)
                .forEach(adventurer -> adventurer.getMovements().forEach(movement -> {
                    switch (movement) {
                        case A -> this.moveAdventurerByOrientation(adventurer);
                        case G -> adventurer.turnLeft();
                        case D -> adventurer.turnRight();
                    }
                }));
    }

    private void moveAdventurerByOrientation(final Adventurer adventurer) {
        switch (adventurer.getOrientation()) {
            case SOUTH -> this.moveAdventurer(adventurer, 0, 1);
            case NORTH -> this.moveAdventurer(adventurer, 0, -1);
            case EAST -> this.moveAdventurer(adventurer, 1, 0);
            case WEST -> this.moveAdventurer(adventurer, -1, 0);
        }
    }

    private void moveAdventurer(final Adventurer adventurer, final int dx, final int dy) {
        final int newHorizontal = adventurer.getHorizontal() + dx;
        final int newVertical = adventurer.getVertical() + dy;

        if (this.isWithinBounds(newHorizontal, newVertical) && !this.isMountainInDestinationPosition(newHorizontal, newVertical)) {
            this.checkIfCanPickUpTreasureAndUpdatePoint(adventurer, newHorizontal, newVertical);
            this.updateAdventurePosition(adventurer, newHorizontal, newVertical);
        }
    }

    private void updateAdventurePosition(final Adventurer adventurer, final int newHorizontal, final int newVertical) {
        adventurer.movePosition(newHorizontal, newVertical);
    }

    private void checkIfCanPickUpTreasureAndUpdatePoint(final Adventurer adventurer, final int newHorizontal, final int newVertical) {
        if (this.isTreasureInDestinationPosition(newHorizontal, newVertical)) {
            adventurer.wonPoint();
            ((Treasure) this.mapGrid[newHorizontal][newVertical]).visited();
        }
    }

    private boolean isMountainInDestinationPosition(final int newHorizontal, final int newVertical) {
        return this.mapGrid[newHorizontal][newVertical] instanceof Mountain;
    }

    private boolean isTreasureInDestinationPosition(final int newHorizontal, final int newVertical) {
        return this.mapGrid[newHorizontal][newVertical] instanceof Treasure;
    }

    private void placeCoordinatesOnMap() {
        this.getAllTreasuresAndMountains().forEach(coordinate -> {
            final int horizontal = coordinate.getHorizontal();
            final int vertical = coordinate.getVertical();
            if (this.isWithinBounds(horizontal, vertical)) {
                this.mapGrid[horizontal][vertical] = coordinate;
            }
        });
    }

    private boolean isWithinBounds(final int horizontal, final int vertical) {
        return horizontal >= 0 &&
                vertical >= 0 &&
                this.getHorizontalCells().horizontalCells() > horizontal &&
                this.getVerticalCells().verticalCells() > vertical;
    }

    private void initializeMapGrid() {
        final int horizontal = this.getHorizontalCells().horizontalCells();
        final int vertical = this.getVerticalCells().verticalCells();
        this.mapGrid = new Coordinate[horizontal][vertical];

        for (int x = 0; x < horizontal; x++) {
            for (int y = 0; y < vertical; y++) {
                this.mapGrid[x][y] = new MyMap(x, y);
            }
        }
    }

    private void validateCoordinatesContentInformation() {
        this.checkIfContainingOnlyMyOwnClass(this.mountains, Mountain.class);
        this.checkIfContainingOnlyMyOwnClass(this.treasures, Treasure.class);
        this.checkIfContainingOnlyMyOwnClass(this.adventurers, Adventurer.class);
    }

    private <T> void checkIfContainingOnlyMyOwnClass(List<?> entities, Class<T> myClass) {
        if (!entities.stream().allMatch(myClass::isInstance)) {
            throw new MapGameClassException(myClass.getSimpleName());
        }
    }

    private List<Coordinate> getAllTreasuresAndMountains() {
        return Stream.of(this.treasures, this.mountains)
                .flatMap(Collection::stream)
                .toList();
    }

    private void checkIfCoordinatesContainsRightValues(final MyMap myMap,
                                                       final Map<String, List<Coordinate>> treasureGameTypeToCoordinates) {
        if (myMap == null) {
            throw new MapGameInformationException("MyMap");
        }

        if (treasureGameTypeToCoordinates == null) {
            throw new MapGameInformationException("TreasureGameTypeToCoordinates");
        }

        if (!treasureGameTypeToCoordinates.containsKey(M)) {
            throw new MapGameInformationException("Mountain");
        }

        if (!treasureGameTypeToCoordinates.containsKey(T)) {
            throw new MapGameInformationException("Treasure");
        }

        if (!treasureGameTypeToCoordinates.containsKey(A)) {
            throw new MapGameInformationException("Adventurer");
        }
    }

    private void fillInAllTheRightInformationInList(final Map<String, List<Coordinate>> treasureGameTypeToCoordinates) {
        this.mountains = treasureGameTypeToCoordinates.get(M);
        this.treasures = treasureGameTypeToCoordinates.get(T);
        this.adventurers = treasureGameTypeToCoordinates.get(A);
    }
}
