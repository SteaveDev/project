package fr.steave.carbon.treasure.domain.model;

public class MyMap extends Coordinate {
    public static final String MAP_GAME_REGEX = "^C - \\d+ - \\d+$";

    public MyMap(final int horizontal, final int vertical) {
        super(horizontal, vertical);
    }

    public static MyMap createMapGame(final String[] information) {
        final int horizontal = Integer.parseInt(information[1]);
        final int vertical = Integer.parseInt(information[2]);

        return new MyMap(horizontal, vertical);
    }

    @Override
    public String toString() {
        return ".";
    }


    @Override
    public boolean equals(final Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
