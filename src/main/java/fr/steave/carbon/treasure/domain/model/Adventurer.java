package fr.steave.carbon.treasure.domain.model;

import fr.steave.carbon.error.Assert;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

public class Adventurer extends Coordinate {
    public static final String ADVENTURER_REGEX = "^A - [a-zA-Z]+ - \\d+ - \\d+ - [NSEO] - [ADG]+$";
    private final AdventurerName name;
    private final List<String> movements;
    private final Orientation[] orientations = {Orientation.NORTH, Orientation.EAST, Orientation.SOUTH, Orientation.WEST};
    private Orientation orientation;
    private int point;

    public Adventurer(final int horizontal, final int vertical, final String name, final String orientation, List<String> movements) {
        super(horizontal, vertical);
        this.name = new AdventurerName(name);
        this.orientation = this.buildOrientation(orientation);
        this.movements = List.copyOf(movements);
        this.point = 0;
    }

    public static Adventurer createAdventurer(final String[] information) {
        final var name = information[1];
        final var horizontal = Integer.parseInt(information[2]);
        final var vertical = Integer.parseInt(information[3]);
        final var orientation = information[4];
        final var movements = Arrays.asList(information[5].split(""));

        return new Adventurer(horizontal, vertical, name, orientation, movements);
    }

    public void movePosition(final int horizontal, final int vertical) {
        super.setHorizontal(horizontal);
        super.setVertical(vertical);
    }

    public void wonPoint() {
        this.point++;
    }

    public int getPoint() {
        return point;
    }

    public Orientation buildOrientation(final String orientation) {
        Assert.builder(this.getClass())
                .notNull(orientation, "orientation")
                .notBlank(orientation, "orientation");

        final char firstCharacter = orientation.charAt(0);
        return switch (firstCharacter) {
            case 'N' -> Orientation.NORTH;
            case 'S' -> Orientation.SOUTH;
            case 'W' -> Orientation.WEST;
            case 'E' -> Orientation.EAST;
            default -> throw new IllegalArgumentException("Orientation invalide: " + orientation);
        };
    }

    public AdventurerName getName() {
        return name;
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public List<String> getMovements() {
        return movements;
    }

    public void turnLeft() {
        int currentIndex = Arrays.asList(orientations).indexOf(this.orientation);
        int newIndex = (currentIndex - 1 + orientations.length) % orientations.length;
        this.orientation = orientations[newIndex];
    }

    public void turnRight() {
        int currentIndex = Arrays.asList(orientations).indexOf(this.orientation);
        int newIndex = (currentIndex + 1) % orientations.length;
        this.orientation = orientations[newIndex];
    }

    @Override
    public String toString() {
        return "A(" + name.name() + ")";
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof final Adventurer that)) return false;
        if (!super.equals(o)) return false;
        return Objects.equals(getName(), that.getName()) && Objects.equals(getMovements(), that.getMovements()) && Arrays.equals(orientations, that.orientations) && getOrientation() == that.getOrientation();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), name, orientation, movements);
    }
}
