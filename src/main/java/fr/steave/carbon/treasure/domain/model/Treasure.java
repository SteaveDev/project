package fr.steave.carbon.treasure.domain.model;

import java.util.Objects;

public class Treasure extends Coordinate {
    public static final String TREASURE_REGEX = "^T - \\d+ - \\d+ - \\d+$";

    private int value;

    public Treasure(final int horizontal, final int vertical, final int value) {
        super(horizontal, vertical);
        this.value = value;
    }

    public static Treasure createTreasure(String[] information) {
        int horizontal = Integer.parseInt(information[1]);
        int vertical = Integer.parseInt(information[2]);
        int value = Integer.parseInt(information[3]);

        return new Treasure(horizontal, vertical, value);
    }

    public int getValue() {
        return value;
    }

    public void visited() {
        if (this.getValue() > 0) {
            this.value--;
        }
    }

    @Override
    public String toString() {
        return "T(" + this.getValue() + ")";
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (!(o instanceof final Treasure treasure)) return false;
        if (!super.equals(o)) return false;
        return getValue() == treasure.getValue();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getValue());
    }
}
