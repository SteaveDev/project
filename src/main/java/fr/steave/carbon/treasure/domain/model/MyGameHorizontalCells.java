package fr.steave.carbon.treasure.domain.model;

import fr.steave.carbon.error.Assert;

public record MyGameHorizontalCells(int horizontalCells) {

    public MyGameHorizontalCells {
        Assert.builder(this.getClass())
                .minValue(horizontalCells, 0,"horizontalCells");
    }

}
