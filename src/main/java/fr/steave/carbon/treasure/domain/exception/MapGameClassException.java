package fr.steave.carbon.treasure.domain.exception;

import static java.lang.String.format;

public class MapGameClassException extends RuntimeException {

    public MapGameClassException(final String information) {
        super(format("%s class information is wrong.", information));
    }

}
