package fr.steave.carbon.treasure.infrastructure;

import fr.steave.carbon.treasure.domain.model.TextFileToUpload;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class TextFileToUploadDtoMapper {

    public TextFileToUpload toModel(MultipartFile file) throws IOException {
        return new TextFileToUpload(file.getContentType(), file.getBytes());
    }

}
