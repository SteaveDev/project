package fr.steave.carbon.treasure.infrastructure;

import fr.steave.carbon.treasure.application.GetFinalResultOfTheTreasureMapUseCase;
import fr.steave.carbon.treasure.domain.model.TextFileToUpload;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping(
        value = "/api/v1/carbon",
        produces = "application/json;charset=UTF-8")
public class TreasureController {

    private final GetFinalResultOfTheTreasureMapUseCase getFinalResultOfTheTreasureMapUseCase;
    private final TextFileToUploadDtoMapper textFileToUploadDtoMapper;

    public TreasureController(
            final GetFinalResultOfTheTreasureMapUseCase getFinalResultOfTheTreasureMapUseCase,
            final TextFileToUploadDtoMapper textFileToUploadDtoMapper
    ) {
        this.getFinalResultOfTheTreasureMapUseCase = getFinalResultOfTheTreasureMapUseCase;
        this.textFileToUploadDtoMapper = textFileToUploadDtoMapper;
    }

    @PostMapping()
    public ResponseEntity<String> getFinalResultOfTheTreasureMapUseCase(
            @RequestParam(name = "file") final MultipartFile file
    ) throws IOException {
        final TextFileToUpload textFileToUpload = this.textFileToUploadDtoMapper.toModel(file);

        this.getFinalResultOfTheTreasureMapUseCase.execute(textFileToUpload);

        return ResponseEntity.ok("Opération réalisée avec succès !");
    }
}
