package fr.steave.carbon.treasure.infrastructure;

import fr.steave.carbon.treasure.domain.exception.DocumentFileServerException;
import fr.steave.carbon.treasure.domain.spi.DocumentStorageSpi;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component
public class DocumentStorageAdapter implements DocumentStorageSpi {

    private static final Logger logger = Logger.getLogger(DocumentStorageAdapter.class.getName());

    @Override
    public void uploadTextFile(final String fileName, final String fileContent) {
        final Path filePath = Paths.get(fileName + ".txt");

        try {
            Files.writeString(filePath, fileContent);
            logger.log(Level.INFO, "File {0} successfully written", filePath);

        } catch (IOException e) {
            final String message = "Failed to write file : " + filePath;
            logger.log(Level.SEVERE, message, e);
            throw new DocumentFileServerException("Failed to write file: " + filePath);
        }

    }
}
