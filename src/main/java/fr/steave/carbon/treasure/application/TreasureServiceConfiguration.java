package fr.steave.carbon.treasure.application;

import fr.steave.carbon.treasure.domain.api.MapGameFileService;
import fr.steave.carbon.treasure.domain.service.MapGameFileServiceImpl;
import fr.steave.carbon.treasure.domain.spi.DocumentStorageSpi;
import fr.steave.carbon.treasure.domain.usecase.GetFinalResultOfTheTreasureMapUseCaseImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TreasureServiceConfiguration {

    @Bean
    public GetFinalResultOfTheTreasureMapUseCase getPriceOfDrinksUseCase(
            final MapGameFileService mapGameFileService,
            final DocumentStorageSpi documentStorageSpi
    ) {
        return new GetFinalResultOfTheTreasureMapUseCaseImpl(
                mapGameFileService,
                documentStorageSpi
        );
    }

    @Bean
    public MapGameFileService mapGameFileService() {
        return new MapGameFileServiceImpl();
    }
}
