package fr.steave.carbon.treasure.application;

import fr.steave.carbon.treasure.domain.model.TextFileToUpload;

public interface GetFinalResultOfTheTreasureMapUseCase {

    void execute(TextFileToUpload textFileToUpload);

}
